package params;
import Scripts.Dscript;
public class constant
{
	
	public static final String URL="https://www.google.com/";
	public static final String browsername="Chrome";
	//chrome driver path
	public static final String Chrome_path="C:\\Program Files (x86)\\Jenkins\\workspace\\chromedriver.exe";
	public static final String Search_field_xpath="//input[@name='q']";
	public static final String Gitlab_xpath="//div[@class='g']//div//div//div[@class='rc']//span[@class='S3Uucc'][contains(text(),'GitLab')]";
	public static final String screenshot_path="D:\\Screenshots\\image.jpg";
}
