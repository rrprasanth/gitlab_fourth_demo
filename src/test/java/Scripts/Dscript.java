package Scripts;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import javax.imageio.ImageIO;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;
import params.constant;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;
public class Dscript
{
	@Test
	public void unit_test() throws IOException
	{
		constant c=new constant();
		System.setProperty("webdriver.chrome.driver",c.Chrome_path );
		WebDriver driver=new ChromeDriver();
		driver.get(c.URL);
		driver.manage().window().maximize();
		driver.findElement(By.xpath(c.Search_field_xpath)).sendKeys("gitlab",Keys.ENTER);
		driver.findElement(By.xpath(c.Gitlab_xpath)).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		System.out.println("Gitlab website is opened successfully");
        //Convert web driver object to TakeScreenshot
		Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(driver);
		ImageIO.write(screenshot.getImage(), "jpg", new File(c.screenshot_path));	
		System.out.println("Screenshot is taken successfully--15.11.2019");
		driver.close();
	}
}
